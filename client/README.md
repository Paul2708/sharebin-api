# Sharebin - Client
---
This project is a Java-Client to resolve pastes and paste your own content in Java.

## Client - Getting started
- Checkout the [main overview](../README.md).

### External jar
- Download the client as external jar [here](https://bitbucket.org/Paul2708/sharebin-api/downloads/sharebin-client.jar "Download link").
- Add the jar to your classpath. ([Eclipse](https://stackoverflow.com/questions/3280353/how-to-import-a-jar-in-eclipse "Tutorial"), 
[IntelliJ](https://stackoverflow.com/questions/1051640/correct-way-to-add-external-jars-lib-jar-to-an-intellij-idea-project "Tutorial"))

### Maven
- Clone the repository `git clone https://Paul2708@bitbucket.org/Paul2708/sharebin-api.git` or download it
- Install it into your local repo `mvn clean install`  
  (Be sure, that you are in `./sharebin-api/` to install it) 
- Add the following dependency:  
  ```xml
   <dependency>
      <groupId>de.paul2708</groupId>
      <artifactId>sharebin-client</artifactId>
      <version>1.0</version>
   </dependency>
  ```

## Client - Usage
### Create a client
```java
SharebinClient client = SharebinAPI.create("api-token");
```

### Paste a paste
```java
try {
    Paste paste = client.paste("This is a sample paste.");
    
    System.out.println("Owner: " + paste.getAuthor());
    System.out.println("Link: https://sbin.tk/?id=" + paste.getCode());
    System.out.println("Timestamp: " + paste.getTimeStamp());
    System.out.println("Content: " + paste.getContent());
} catch (ErrorResponseException e) {
    e.printStackTrace();
}
```

### Resolve pastes
You can sort the pastes by `TIME`, `ID` and `LENGTH`.  

```java
try {
    List<Paste> pastes = client.resolvePastes(SortType.TIME);

    for (Paste paste : pastes) {
        System.out.println(paste);
    }
} catch (ErrorResponseException e) {
    e.printStackTrace();
}
```

### Error handling
If you are using the API, some errors can occure. Have a look at the [error overview](../common/README.md).  
You can get the error object from the `ErrorResponseException`and handle it.  

```java
try {
    // Any request
} catch (ErrorResponseException e) {
    ErrorResponse error = e.getErrorResponse();

    System.out.println("Id: " + error.getId());
    System.out.println("Error: " + error.getError());
    System.out.println("Description: " + error.getDescription());
            
    if (error.equals(ErrorResponse.PASTE_TOO_LONG)) {
        System.out.println("Your paste is too big.");
    }
}
```

### Async Client
The asynchronous client uses a normal `SharebinClient` with an [`ExecuterService`](https://docs.oracle.com/javase/7/docs/api/java/util/concurrent/ExecutorService.html "JavaDoc").  
When you don't use the client anymore, use `client.close()` to stop the service.  

You will get your answer in a `Result`. It is an interface with two methods:  

- `void result(T result)`: called if the request came in
- `void error(ErrorResponseException exception)`: called if an error occurred

#### Create client
```java
AsyncSharebinClient client = SharebinAPI.createAsync("api-token");
```

#### Paste a paste
```java
client.paste("This is a simple paste.", new Result<Paste>() {
            
    @Override
    public void result(Paste result) {
        System.out.println("Owner: " + result.getAuthor());
        System.out.println("Link: https://sbin.tk/?id=" + result.getCode());
        System.out.println("Timestamp: " + result.getTimeStamp());
        System.out.println("Content: " + result.getContent());
    }

    @Override
    public void error(ErrorResponseException exception) {
         exception.printStackTrace();
    }
});
```

#### Resolve pastes
You can sort the pastes by `TIME`, `ID` and `LENGTH`.  

```java
client.resolvePastes(SortType.LENGTH, new Result<List<Paste>>() {
            
    @Override
    public void result(List<Paste> result) {
        for (Paste paste : result) {
            System.out.println(paste);
        }
    }

    @Override
    public void error(ErrorResponseException exception) {
        exception.printStackTrace();
    }
});
```

#### Close the api
You have the close the api after usage.

```java
client.close();
```

#### Error handling
If you are using the API, some errors can occure. Have a look at the [error overview](../common/README.md).  
You can get the error object from the `ErrorResponseException` and handle it.  

```java
new Result<Paste>() {
            
    @Override
    public void result(Paste result) {
        // Handle result
    }

    @Override
    public void error(ErrorResponseException exception) {
        ErrorResponse error = exception.getErrorResponse();

        System.out.println("Id: " + error.getId());
        System.out.println("Error: " + error.getError());
        System.out.println("Description: " + error.getDescription());

        if (error.equals(ErrorResponse.PASTE_TOO_LONG)) {
            System.out.println("Your paste is too big.");
        }
    }
};
```

## Client - Contact
If you find any issues, please [contact](../README.md) us!

