package de.paul2708.client.client.comparator;

import de.paul2708.common.paste.Paste;

import java.util.Comparator;

/**
 * This comparator doesn't sort the pastes.
 *
 * @author Paul2708
 */
public class NoneComparator implements Comparator<Paste> {

    private static final int CONSTANT = 1;

    /**
     * Return constant <code>1</code>.
     *
     * @param paste first paste
     * @param otherPaste second paste
     * @return <code>1</code>
     */
    @Override
    public int compare(Paste paste, Paste otherPaste) {
        return NoneComparator.CONSTANT;
    }
}
