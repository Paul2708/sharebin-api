package de.paul2708.client.client.comparator;

import de.paul2708.common.paste.Paste;
import java.util.Comparator;

/**
 * This comparator compares to pastes by its code/id.
 * The pastes will be sort lexicographically.
 *
 * @author Paul2708
 * @see java.lang.String#compareTo(String)
 */
public class CodeComparator implements Comparator<Paste> {

    /**
     * Compare two pastes by its code/id. It will sort by using {@link java.lang.String#compareTo(String)}.
     *
     * @param paste first paste
     * @param otherPaste second paste
     * @return the compared value
     */
    @Override
    public int compare(Paste paste, Paste otherPaste) {
        return paste.getCode().compareTo(otherPaste.getCode());
    }
}
