package de.paul2708.client.client.comparator;

import de.paul2708.common.paste.Paste;
import java.util.Comparator;

/**
 * This comparator compares to pastes by its length.
 * The first paste is the shortest paste.
 *
 * @author Paul2708
 */
public class LengthComparator implements Comparator<Paste> {

    /**
     * Compare two pastes by its length. It will sort from smallest to longest.
     *
     * @param paste first paste
     * @param otherPaste second paste
     * @return the compared value
     */
    @Override
    public int compare(Paste paste, Paste otherPaste) {
        int firstLength = paste.getContent().length();
        int secondLength = otherPaste.getContent().length();

        return Integer.compare(firstLength, secondLength);
    }
}
