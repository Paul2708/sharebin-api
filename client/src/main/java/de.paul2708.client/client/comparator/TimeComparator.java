package de.paul2708.client.client.comparator;

import de.paul2708.common.paste.Paste;
import java.util.Comparator;

/**
 * This comparator compares to pastes by its time stamps.
 * The first paste is the latest created paste.
 *
 * @author Paul2708
 */
public class TimeComparator implements Comparator<Paste> {

    /**
     * Compare two pastes by its time stamps. It will sort from latest to oldest.
     *
     * @param paste first paste
     * @param otherPaste second paste
     * @return the compared value
     */
    @Override
    public int compare(Paste paste, Paste otherPaste) {
        long firstTimeStamp = paste.resolveTimeStampDate().getTime();
        long secondTimeStamp = otherPaste.resolveTimeStampDate().getTime();

        return Long.compare(secondTimeStamp, firstTimeStamp);
    }
}
