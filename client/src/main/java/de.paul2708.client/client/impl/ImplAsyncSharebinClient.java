package de.paul2708.client.client.impl;

import de.paul2708.client.client.AsyncSharebinClient;
import de.paul2708.client.client.SharebinClient;
import de.paul2708.client.client.SortType;
import de.paul2708.client.client.util.Result;
import de.paul2708.client.exception.ErrorResponseException;
import de.paul2708.common.paste.Paste;
import de.paul2708.common.rest.response.ErrorResponse;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * This class is the concrete implementation of the {@link de.paul2708.client.client.AsyncSharebinClient}.<br>
 * It's a {@link ImplSharebinClient} with an {@link ExecutorService} for asynchronous execution.
 *
 * @author Paul2708
 */
public class ImplAsyncSharebinClient implements AsyncSharebinClient {

    private SharebinClient client;

    private ExecutorService executorService;

    /**
     * Create a new async sharebin client implementation.
     *
     * @param token api token
     */
    public ImplAsyncSharebinClient(String token) {
        this.client = new ImplSharebinClient(token);

        this.executorService = Executors.newCachedThreadPool();
    }

    /**
     * Resolve all pastes from your account (api token related).
     * You can sort it by different {@link de.paul2708.client.client.SortType}s.
     *
     * @param sortType sort type
     * @param result result with a list of pastes
     */
    @Override
    public void resolvePastes(SortType sortType, Result<List<Paste>> result) {
        if (executorService.isShutdown()) {
            result.error(new ErrorResponseException(ErrorResponse.API_CLOSED));
            return;
        }

        executorService.execute(() -> {
            try {
                List<Paste> pastes = client.resolvePastes(sortType);

                result.result(pastes);
            } catch (ErrorResponseException e) {
                result.error(e);
            }
        });
    }

    /**
     * Create a new paste and paste it to your account (api token related).
     * Reminder: Your paste has limited chars! See {@link de.paul2708.common.paste.Paste#MAX_LENGTH}.
     *
     * @param content  paste content
     * @param result result with the created paste
     */
    @Override
    public void paste(String content, Result<Paste> result) {
        if (executorService.isShutdown()) {
            result.error(new ErrorResponseException(ErrorResponse.API_CLOSED));
            return;
        }

        executorService.execute(() -> {
            try {
                Paste paste = client.paste(content);

                result.result(paste);
            } catch (ErrorResponseException e) {
                result.error(e);
            }
        });
    }

    /**
     * Close the executor service, if you are no longer using the api.
     * Your program won't terminate without calling this method.
     */
    @Override
    public void close() {
        executorService.shutdown();
    }
}
