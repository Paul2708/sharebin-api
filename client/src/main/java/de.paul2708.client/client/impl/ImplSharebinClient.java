package de.paul2708.client.client.impl;

import de.paul2708.client.client.SharebinClient;
import de.paul2708.client.client.SortType;
import de.paul2708.client.exception.ErrorResponseException;
import de.paul2708.common.paste.Paste;
import de.paul2708.common.rest.response.ErrorResponse;
import de.paul2708.common.rest.response.PasteCreationResponse;
import de.paul2708.common.rest.response.PasteListResponse;
import de.paul2708.common.rest.response.ResourceResponse;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collections;
import java.util.List;

/**
 * This class is the concrete implementation of the {@link de.paul2708.client.client.SharebinClient}.
 *
 * @author Paul2708
 */
public class ImplSharebinClient implements SharebinClient {

    private static final String PASTES_TARGET = "http://api.sbin.tk/pastes/";

    private static final String CREATE_TARGET = "http://api.sbin.tk/pastes/create";

    private String token;

    /**
     * Create a new sharebin client implementation.
     *
     * @param token api token
     */
    public ImplSharebinClient(String token) {
        this.token = token;
    }

    /**
     * Resolve all pastes from your account (api token related).
     * You can sort it by different {@link de.paul2708.client.client.SortType}s.
     *
     * @param sortType sort type
     * @return list of pastes
     * @throws ErrorResponseException if the request wasn't successful and a
     * {@link de.paul2708.common.rest.response.ErrorResponse} came back
     */
    @Override
    public List<Paste> resolvePastes(SortType sortType) throws ErrorResponseException {
        try {
            URL url = new URL(ImplSharebinClient.PASTES_TARGET);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.4; en-US; "
                    + "rv:1.9.2.2) Gecko/20100316 Firefox/3.6.2");
            connection.setRequestMethod("GET");
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setUseCaches(false);
            connection.setConnectTimeout(5000);
            connection.setReadTimeout(5000);
            connection.setRequestProperty("X-Api-Token", token);

            if (connection.getResponseCode() == ErrorResponse.INVALID_API_TOKEN.getHtmlStatus()) {
                throw new ErrorResponseException(ErrorResponse.INVALID_API_TOKEN);
            }
            if (connection.getResponseCode() == ErrorResponse.LIMIT_REACHED.getHtmlStatus()) {
                throw new ErrorResponseException(ErrorResponse.LIMIT_REACHED);
            }

            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String jsonResponse = reader.readLine();

            ResourceResponse resourceResponse = ResourceResponse.parse(jsonResponse);

            reader.close();

            if (resourceResponse instanceof PasteListResponse) {
                List<Paste> pastes = ((PasteListResponse) resourceResponse).getPastes();

                Collections.sort(pastes, sortType.getComparator());

                return pastes;
            } else if (resourceResponse instanceof ErrorResponse) {
                throw new ErrorResponseException((ErrorResponse) resourceResponse);
            } else {
                throw new ErrorResponseException(ErrorResponse.INTERNAL_EXCEPTION);
            }
        } catch (IOException e) {
            throw new ErrorResponseException(ErrorResponse.REQUEST_PROCESS);
        }
    }

    /**
     * Create a new paste and paste it to your account (api token related).
     * Reminder: Your paste has limited chars! See {@link de.paul2708.common.paste.Paste#MAX_LENGTH}.
     *
     * @param content paste content
     * @return created paste
     * @throws ErrorResponseException if the request wasn't successful and a
     * {@link de.paul2708.common.rest.response.ErrorResponse} came back
     */
    @Override
    public Paste paste(String content) throws ErrorResponseException {
        if (content.length() >= Paste.MAX_LENGTH) {
            throw new ErrorResponseException(ErrorResponse.PASTE_TOO_LONG);
        }

        try {
            URL url = new URL(ImplSharebinClient.CREATE_TARGET);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.4; en-US; "
                    + "rv:1.9.2.2) Gecko/20100316 Firefox/3.6.2");
            connection.setRequestMethod("POST");
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setUseCaches(false);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(5000);
            connection.setReadTimeout(5000);
            connection.setRequestProperty("X-Api-Token", token);

            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
            writer.write("{\"content\": \"" + content + "\"}");
            writer.flush();

            if (connection.getResponseCode() == ErrorResponse.INVALID_API_TOKEN.getHtmlStatus()) {
                throw new ErrorResponseException(ErrorResponse.INVALID_API_TOKEN);
            }
            if (connection.getResponseCode() == ErrorResponse.LIMIT_REACHED.getHtmlStatus()) {
                throw new ErrorResponseException(ErrorResponse.LIMIT_REACHED);
            }

            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String jsonResponse = reader.readLine();

            ResourceResponse resourceResponse = ResourceResponse.parse(jsonResponse);

            writer.close();
            reader.close();

            if (resourceResponse instanceof PasteCreationResponse) {
                return ((PasteCreationResponse) resourceResponse).getPaste();
            } else if (resourceResponse instanceof ErrorResponse) {
                throw new ErrorResponseException((ErrorResponse) resourceResponse);
            } else {
                throw new ErrorResponseException(ErrorResponse.INTERNAL_EXCEPTION);
            }
        } catch (IOException e) {
            throw new ErrorResponseException(ErrorResponse.REQUEST_PROCESS);
        }
    }
}
