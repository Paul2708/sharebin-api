package de.paul2708.client.client;

import de.paul2708.client.client.util.Result;
import de.paul2708.common.paste.Paste;
import java.util.List;

/**
 * This interface represents the asynchronous sharebin client.
 * You can create an instance by using {@link de.paul2708.client.SharebinAPI#createAsync(String)}.
 * It will work like the {@link de.paul2708.client.client.SharebinClient} with callbacks.
 *
 * @author Paul2708
 */
public interface AsyncSharebinClient {

    /**
     * Resolve all pastes from your account (api token related).
     * You can sort it by different {@link de.paul2708.client.client.SortType}s.
     *
     * @param sortType sort type
     * @param result result with a list of pastes
     */
    void resolvePastes(SortType sortType, Result<List<Paste>> result);

    /**
     * Create a new paste and paste it to your account (api token related).
     * Reminder: Your paste has limited chars! See {@link de.paul2708.common.paste.Paste#MAX_LENGTH}.
     *
     * @param content paste content
     * @param result result with the created paste
     */
    void paste(String content, Result<Paste> result);

    /**
     * Close the executor service, if you are no longer using the api.
     * Your program won't terminate without calling this method.
     */
    void close();
}
