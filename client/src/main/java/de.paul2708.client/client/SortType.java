package de.paul2708.client.client;

import de.paul2708.client.client.comparator.CodeComparator;
import de.paul2708.client.client.comparator.LengthComparator;
import de.paul2708.client.client.comparator.NoneComparator;
import de.paul2708.client.client.comparator.TimeComparator;
import de.paul2708.common.paste.Paste;
import java.util.Comparator;

/**
 * This enum represents all types of sorting with its comparator.
 *
 * @author Paul2708
 */
public enum SortType {

    /**
     * There is no special order.
     */
    NONE(new NoneComparator()),

    /**
     * Default value. The pastes will be sort by time. The first paste will be the latest paste.
     */
    TIME(new TimeComparator()),

    /**
     * Sort the pastes from short to long.
     */
    LENGTH(new LengthComparator()),

    /**
     * Sort the pastes by its id.
     *
     * @see java.lang.String#compareTo(String)
     */
    ID(new CodeComparator());

    private Comparator<Paste> comparator;

    /**
     * Create a new sort type with its comparator.
     *
     * @param comparator sort comparator
     */
    SortType(Comparator<Paste> comparator) {
        this.comparator = comparator;
    }

    /**
     * Get the sort comparator.
     *
     * @return comparator
     */
    public Comparator<Paste> getComparator() {
        return comparator;
    }
}
