package de.paul2708.client.client.util;

import de.paul2708.client.exception.ErrorResponseException;

/**
 * This interface represents a request result, that can handle the result an error.
 *
 * @param <T> result
 * @author Paul2708
 */
public interface Result<T> {

    /**
     * Handle the request response.
     *
     * @param result incoming result
     */
    void result(T result);

    /**
     * Handle an exception, if any exception occurred.
     *
     * @param exception occurred exception
     */
    void error(ErrorResponseException exception);
}
