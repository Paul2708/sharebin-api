package de.paul2708.client.client;

import de.paul2708.client.exception.ErrorResponseException;
import de.paul2708.common.paste.Paste;
import java.util.List;

/**
 * This interface represents the sharebin client.
 * You can create an instance by using {@link de.paul2708.client.SharebinAPI#create(String)}.
 *
 * @author Paul2708
 */
public interface SharebinClient {

    /**
     * Resolve all pastes from your account (api token related).
     * You can sort it by different {@link de.paul2708.client.client.SortType}s.
     *
     * @param sortType sort type
     * @return list of pastes
     * @throws ErrorResponseException if the request wasn't successful and a
     * {@link de.paul2708.common.rest.response.ErrorResponse} came back
     */
    List<Paste> resolvePastes(SortType sortType) throws ErrorResponseException;

    /**
     * Create a new paste and paste it to your account (api token related).
     * Reminder: Your paste has limited chars! See {@link de.paul2708.common.paste.Paste#MAX_LENGTH}.
     *
     * @param content paste content
     * @return created paste
     * @throws ErrorResponseException if the request wasn't successful and a
     * {@link de.paul2708.common.rest.response.ErrorResponse} came back
     */
    Paste paste(String content) throws ErrorResponseException;
}
