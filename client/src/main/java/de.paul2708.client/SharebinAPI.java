package de.paul2708.client;

import de.paul2708.client.client.AsyncSharebinClient;
import de.paul2708.client.client.SharebinClient;
import de.paul2708.client.client.impl.ImplAsyncSharebinClient;
import de.paul2708.client.client.impl.ImplSharebinClient;

/**
 * This class represents the sharebin api and holds methods to create new client instances.
 *
 * @author Paul2708
 */
public final class SharebinAPI {

    /**
     * Nothing to call here..
     */
    private SharebinAPI() {
        throw new IllegalAccessError();
    }

    /**
     * Create a new sharebin client instance. The token will be used in the called methods.
     *
     * @param token api token
     * @return new sharebin client
     */
    public static SharebinClient create(String token) {
        return new ImplSharebinClient(token);
    }

    /**
     * Create a new asynchronous sharebin client instance. The token will be used in the called methods.
     *
     * @param token api token
     * @return new async sharebin client
     */
    public static AsyncSharebinClient createAsync(String token) {
        return new ImplAsyncSharebinClient(token);
    }
}
