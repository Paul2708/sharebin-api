package de.paul2708.client.exception;

import de.paul2708.common.rest.response.ErrorResponse;

/**
 * This exception is thrown, if an error response comes in.
 *
 * @author Paul2708
 */
public class ErrorResponseException extends Exception {

    private ErrorResponse errorResponse;

    /**
     * Create a new error response exception.
     *
     * @param errorResponse error response
     */
    public ErrorResponseException(ErrorResponse errorResponse) {
        super(errorResponse.getError() + ": " + errorResponse.getDescription());

        this.errorResponse = errorResponse;
    }

    /**
     * Get the error response.
     *
     * @return error response
     */
    public ErrorResponse getErrorResponse() {
        return errorResponse;
    }
}
