package de.paul2708.common.paste;

import java.io.IOException;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.simple.JsonObject;
import org.json.simple.Jsonable;

/**
 * This class is a model for a paste.
 *
 * @author Paul2708
 */
public class Paste implements Jsonable {

    /**
     * Maximum length of a paste.
     */
    public static final int MAX_LENGTH = 40000;

    /**
     * Simple time stamp format.
     */
    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy - HH:mm");

    private String code;

    private String author;
    private long timeStamp;
    private String content;

    /**
     * Create a new paste with code, author, time stamp and content.
     *
     * @param code paste code
     * @param author paste author
     * @param timeStamp created time stamp
     * @param content paste content
     */
    public Paste(String code, String author, long timeStamp, String content) {
        this.code = code;
        this.author = author;
        this.timeStamp = timeStamp;
        this.content = content;
    }

    /**
     * Get the paste code.
     *
     * @return code
     */
    public String getCode() {
        return code;
    }

    /**
     * Get the paste author.
     *
     * @return author
     */
    public String getAuthor() {
        return author;
    }

    /**
     * Get the created time stamp.
     *
     * @return time stamp
     */
    public long getTimeStamp() {
        return timeStamp;
    }

    /**
     * Get paste content.
     *
     * @return content
     */
    public String getContent() {
        return content;
    }

    /**
     * Resolve the {@link java.util.Date} from the given time stamp.
     *
     * @return date of creation
     */
    public Date resolveTimeStampDate() {
        return new Date(timeStamp);
    }

    /**
     * Transform a paste to a json formatted string.
     *
     * @return paste as json string
     */
    public String toJson() {
        JsonObject object = new JsonObject();

        object.put("code", code);
        object.put("author", author);
        object.put("timestamp", timeStamp);
        object.put("content", content);

        return object.toJson();
    }

    /**
     * Transfer the paste to a json formatted stream.
     *
     * @param writer writer
     * @throws IOException if an exception occurred while writing
     */
    @Override
    public void toJson(Writer writer) throws IOException {
        writer.write(toJson());

        writer.flush();
    }

    /**
     * Get the paste data as string.
     *
     * @return paste as string
     */
    @Override
    public String toString() {
        return "Paste{"
                + "code='" + code + '\''
                + ", author='" + author + '\''
                + ", timeStamp='" + timeStamp + '\''
                + ", content='" + content + '\''
                + '}';
    }
}
