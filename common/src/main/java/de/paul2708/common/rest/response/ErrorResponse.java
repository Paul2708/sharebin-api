package de.paul2708.common.rest.response;

import de.paul2708.common.paste.Paste;
import org.json.simple.JsonObject;
import org.json.simple.Jsonable;

/**
 * This response is a detailed response with error code, error message and description.
 *
 * @author Paul2708
 */
public class ErrorResponse extends ResourceResponse {

    private static final int REQUEST_LIMIT = 20;

    /**
     * Response, if the header doesn't contain the api token.
     */
    public static final ErrorResponse MISSING_API_TOKEN =
            new ErrorResponse(0, 403, "Missing api token", "Every request needs the api token as header value.");

    /**
     * Response, if the database throws any exception.
     */
    public static final ErrorResponse DATABASE_EXCEPTION =
            new ErrorResponse(1, 500, "Database exception",
                    "An exception occurred while interacting with the database. Please inform an admin.");

    /**
     * Response, if the paste has an invalid format.
     */
    public static final ErrorResponse INVALID_PASTE =
            new ErrorResponse(2, 400, "Invalid paste",
                    "Your paste has an invalid format. Please checkout the wiki for detailed information.");

    /**
     * Response, if the paste content is too long.
     */
    public static final ErrorResponse PASTE_TOO_LONG =
            new ErrorResponse(3, 400, "Paste too long",
                    "Your paste is too long. You are only allowed to have " + Paste.MAX_LENGTH + " chars.");

    /**
     * Response, if an internal exception occurs.
     */
    public static final ErrorResponse INTERNAL_EXCEPTION =
            new ErrorResponse(4, 500, "Internal error",
                    "An internal error occurred. Please inform an admin.");

    /**
     * Response, if the api token is invalid.
     */
    public static final ErrorResponse INVALID_API_TOKEN =
            new ErrorResponse(5, 403, "Invalid api token",
                    "Your entered api token is not valid. Please check it again.");

    /**
     * Response, if the maximum requests per minute were reached.
     */
    public static final ErrorResponse LIMIT_REACHED =
            new ErrorResponse(6, 429, "Limit reached",
                    "You are only allowed to send " + ErrorResponse.REQUEST_LIMIT + " requests per minute.");

    /**
     * Response, if the url is not supported.
     */
    public static final ErrorResponse INVALID_URL =
            new ErrorResponse(7, 404, "Invalid url", "This url is not supported.");

    /**
     * Response, if the method is not supported.
     */
    public static final ErrorResponse INVALID_METHOD =
            new ErrorResponse(8, 405, "Invalid method", "This method is not supported.");

    /**
     * Response, if the method is not supported.
     */
    public static final ErrorResponse API_CLOSED =
            new ErrorResponse(9, 400, "Api already closed", "The api client was already closed.");

    /**
     * Response, if the request processing wasn't successful.
     */
    public static final ErrorResponse REQUEST_PROCESS =
            new ErrorResponse(10, 503, "Request wasn't successful", "Is the server offline?");

    private int id;
    private int htmlStatus;
    private String error;
    private String description;

    /**
     * Default constructor.
     */
    public ErrorResponse() {

    }

    /**
     * Create a new detailed response.
     *
     * @param id internal error code
     * @param htmlStatus html status
     * @param error error message
     * @param description error description
     */
    public ErrorResponse(int id, int htmlStatus, String error, String description) {
        super(htmlStatus);

        this.id = id;
        this.htmlStatus = htmlStatus;
        this.error = error;
        this.description = description;
    }

    /**
     * Return a json object with id, error and description.
     *
     * @return jsonable
     */
    @Override
    public Jsonable createJsonResponse() {
        JsonObject object = new JsonObject();

        object.put("id", id);
        object.put("html-status", htmlStatus);
        object.put("error", error);
        object.put("description", description);

        return object;
    }

    /**
     * Check if the given (json) object is valid as response.
     *
     * @param object parsed object
     * @return true if the object is valid, otherwise false
     */
    @Override
    public boolean isValid(Object object) {
        if (object instanceof JsonObject) {
            JsonObject jsonObject = (JsonObject) object;

            return jsonObject.getInteger("id") != null && jsonObject.getString("error") != null
                    && jsonObject.getString("description") != null;
        }

        return false;
    }

    /**
     * Build the response from a json object.
     *
     * @param object parsed object
     * @return new resource response
     */
    @Override
    public ResourceResponse buildFromJson(Object object) {
        JsonObject jsonObject = (JsonObject) object;

        return new ErrorResponse(jsonObject.getInteger("id"), jsonObject.getInteger("html-status"),
                jsonObject.getString("error"), jsonObject.getString("description"));
    }

    /**
     * Get the error id.
     *
     * @return error id.
     */
    public int getId() {
        return id;
    }

    /**
     * Get the error message.
     *
     * @return error message
     */
    public String getError() {
        return error;
    }

    /**
     * Get the description of the error.
     *
     * @return error description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Two error responses are equal, if the id, and only the id, is the same.
     *
     * @param o object to check
     * @return true if the responses are equal, otherwise false
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ErrorResponse response = (ErrorResponse) o;

        return id == response.id;
    }

    /**
     * Return the unique id as hash code.
     *
     * @return id
     */
    @Override
    public int hashCode() {
        return id;
    }

    /**
     * Return the error response as string with id, html status, error and description.
     *
     * @return error response information
     */
    @Override
    public String toString() {
        return "ErrorResponse{"
                + "id=" + id
                + ", htmlStatus=" + htmlStatus
                + ", error='" + error + '\''
                + ", description='" + description + '\''
                + '}';
    }
}
