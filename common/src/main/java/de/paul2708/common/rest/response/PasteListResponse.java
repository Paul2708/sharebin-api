package de.paul2708.common.rest.response;

import de.paul2708.common.paste.Paste;
import java.util.ArrayList;
import java.util.List;
import org.json.simple.JsonArray;
import org.json.simple.JsonObject;
import org.json.simple.Jsonable;

/**
 * This response is used to return all pastes.
 *
 * @author Paul2708
 */
public class PasteListResponse extends ResourceResponse {

    private List<Paste> pastes;

    /**
     * Default constructor.
     */
    public PasteListResponse() {

    }

    /**
     * Create a new paste response with a list of pastes.
     *
     * @param pastes list of pastes
     */
    public PasteListResponse(List<Paste> pastes) {
        super(200);

        this.pastes = pastes;
    }

    /**
     * Get the json array of pastes.
     *
     * @return paste
     */
    @Override
    public Jsonable createJsonResponse() {
        JsonArray array = new JsonArray();

        array.addAll(pastes);

        return array;
    }

    /**
     * Check if the given (json) object is valid as response.
     *
     * @param object parsed object
     * @return true if the object is valid, otherwise false
     */
    @Override
    public boolean isValid(Object object) {
        return object instanceof JsonArray;
    }

    /**
     * Build the response from a json object.
     *
     * @param object parsed object
     * @return new resource response
     */
    @Override
    public ResourceResponse buildFromJson(Object object) {
        JsonArray array = (JsonArray) object;
        List<Paste> pastes = new ArrayList<>();

        for (Object anArray : array) {
            JsonObject pasteObject = (JsonObject) anArray;

            Paste paste = new Paste(pasteObject.getString("code"), pasteObject.getString("author"),
                    pasteObject.getLong("timestamp"), pasteObject.getString("content"));
            pastes.add(paste);
        }

        return new PasteListResponse(pastes);
    }

    /**
     * Get the list of pastes.
     *
     * @return list of pastes
     */
    public List<Paste> getPastes() {
        return pastes;
    }
}
