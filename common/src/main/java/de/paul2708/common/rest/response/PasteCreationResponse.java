package de.paul2708.common.rest.response;

import de.paul2708.common.paste.Paste;
import org.json.simple.JsonObject;
import org.json.simple.Jsonable;

/**
 * This response is used to inform the user about the paste creation.
 *
 * @author Paul2708
 */
public class PasteCreationResponse extends ResourceResponse {

    private Paste paste;

    /**
     * Default constructor.
     */
    public PasteCreationResponse() {

    }

    /**
     * Create a new create response.
     *
     * @param paste created paste
     */
    public PasteCreationResponse(Paste paste) {
        super(201);

        this.paste = paste;
    }

    /**
     * Get the paste as it already is a {@link org.json.simple.Jsonable}.
     *
     * @return paste
     */
    @Override
    public Jsonable createJsonResponse() {
        return paste;
    }

    /**
     * Check if the given (json) object is valid as response.
     *
     * @param object parsed object
     * @return true if the object is valid, otherwise false
     */
    @Override
    public boolean isValid(Object object) {
        if (object instanceof JsonObject) {
            JsonObject jsonObject = (JsonObject) object;

            return jsonObject.getString("code") != null && jsonObject.getString("author") != null
                    && jsonObject.getLong("timestamp") != null;
        }

        return false;
    }

    /**
     * Build the response from a json object.
     *
     * @param object parsed object
     * @return new resource response
     */
    @Override
    public ResourceResponse buildFromJson(Object object) {
        JsonObject jsonObject = (JsonObject) object;

        Paste paste = new Paste(jsonObject.getString("code"), jsonObject.getString("author"),
                jsonObject.getLong("timestamp"), jsonObject.getString("content"));
        return new PasteCreationResponse(paste);
    }

    /**
     * Get the created paste.
     *
     * @return created paste
     */
    public Paste getPaste() {
        return paste;
    }
}
