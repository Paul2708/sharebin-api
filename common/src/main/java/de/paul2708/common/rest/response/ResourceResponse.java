package de.paul2708.common.rest.response;

import org.json.simple.DeserializationException;
import org.json.simple.Jsonable;
import org.json.simple.Jsoner;

import java.lang.reflect.InvocationTargetException;

/**
 * This class represents a response and holds a template method to build the response by using the hook method.
 *
 * @author Paul2708
 */
public abstract class ResourceResponse {

    private static final Class[] RESPONSES = new Class<?>[] { PasteCreationResponse.class, PasteListResponse.class,
            ErrorResponse.class};

    private int htmlStatus;

    /**
     * Default constructor.
     */
    public ResourceResponse() {

    }

    /**
     * Create a new resource response
     *
     * @param htmlStatus html code
     */
    public ResourceResponse(int htmlStatus) {
        this.htmlStatus = htmlStatus;
    }

    /**
     * Get the response as jsonable. (e.g. {@link org.json.simple.JsonObject}).
     *
     * @return jsonable
     */
    public abstract Jsonable createJsonResponse();

    /**
     * Check if the given (json) object is valid as response.
     *
     * @param object parsed object
     * @return true if the object is valid, otherwise false
     */
    public abstract boolean isValid(Object object);

    /**
     * Build the response from a json object.
     *
     * @param object parsed object
     * @return new resource response
     */
    public abstract ResourceResponse buildFromJson(Object object);

    /**
     * Get the html status code.
     *
     * @return html status code
     */
    public int getHtmlStatus() {
        return htmlStatus;
    }

    /**
     * Parse a json string and return the matching response.
     *
     * @param json parsed json string
     * @return the matching response or {@link ErrorResponse#INTERNAL_EXCEPTION} if nothing is matching or
     *          an exception occurs
     */
    public static ResourceResponse parse(String json) {
        try {
            Object jsonObject = Jsoner.deserialize(json);

            for (Class<?> clazz : RESPONSES) {
                try {
                    ResourceResponse resourceResponse = (ResourceResponse) clazz.getConstructor().newInstance();

                    if (resourceResponse.isValid(jsonObject)) {
                        return resourceResponse.buildFromJson(jsonObject);
                    }
                } catch (InstantiationException | IllegalAccessException | InvocationTargetException
                        | NoSuchMethodException e) {
                    e.printStackTrace();
                }
            }
        } catch (DeserializationException e) {
            e.printStackTrace();
        }

        return ErrorResponse.INTERNAL_EXCEPTION;
    }
}
