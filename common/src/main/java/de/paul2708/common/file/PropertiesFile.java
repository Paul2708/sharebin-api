package de.paul2708.common.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

/**
 * This class represents a properties file and provides methods to set and get properties.
 *
 * @author Paul2708
 */
public abstract class PropertiesFile {

    private static final String DEFAULT_COMMENT = "Default properties comment";

    private String path;
    private String comment;

    private File file;
    private Properties properties;

    /**
     * Create a new properties file with path and comment.
     *
     * @param path path to the file, including file name
     * @param comment header in the file
     */
    public PropertiesFile(String path, String comment) {
        this.path = path;
        this.comment = comment;

        this.properties = new Properties();
    }

    /**
     * Create a new properties file with path and standard comment.
     *
     * @param path path to the file, including file name
     */
    public PropertiesFile(String path) {
        this(path, PropertiesFile.DEFAULT_COMMENT);
    }

    /**
     * Load the default properties value.
     *
     * @return default properties value
     */
    public abstract Properties defaultValue();

    /**
     * Validate the values.
     * 
     * @throws InvalidValueException if a value is invalid
     */
    public abstract void validate() throws InvalidValueException;

    /**
     * Load the properties file if it exists, or create a new file with default value.
     *
     * @see #defaultValue()
     * @throws InvalidValueException if {@link #validate()} throws an exception
     */
    public void load() throws InvalidValueException {
        try {
            Path path = Paths.get(this.path);
            this.file = path.toFile();

            if (Files.notExists(path)) {
                if (path.getParent() != null && Files.notExists(path.getParent())) {
                    Files.createDirectories(path.getParent());
                }

                Files.createFile(path);

                this.properties = defaultValue();

                save();
            } else {
                properties.load(new FileInputStream(file));
            }
            
            this.validate();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Set a property.
     *
     * @param key key
     * @param value new value
     */
    public void set(String key, String value) {
        properties.setProperty(key, value);
    }

    /**
     * Get the property by key.
     *
     * @param key property key.
     * @return property
     */
    public String get(String key) {
        return properties.getProperty(key);
    }

    /**
     * Save the (changed) properties to file.
     */
    public void save() {
        try {
            FileOutputStream outputStream = new FileOutputStream(file);
            properties.store(outputStream, comment);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
