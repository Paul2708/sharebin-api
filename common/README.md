# Sharebin - Common
---
This project holds common classes, that are used by [Server](../server) and [Client](../client). For example paste information or request responses.

## Common - Content
- `PropertiesFile` to save [Properties](https://docs.oracle.com/javase/8/docs/api/java/util/Properties.html "Javadoc Properties") in a file and load it
- `ResourceResponse` represents every request answer (Errors, Pastes, etc.)
- `Paste` is the model for a paste with content, author, etc.

## Common - Errors
If an error occurs, you will get the following answer:  
```json
{
  "id": 0,
  "error": "Missing api token",
  "description": "Every request needs the api token as header value.",
  "html-status": 403
}
```
`id` is the internal error identification. You will find a table below.  
`error` is the name of the error.  
`description` describes the error and why it occurred.  
`html-status` represents the [HTTP status code](https://en.wikipedia.org/wiki/List_of_HTTP_status_codes "Wikipedia").

### Errors

| Id | Error                     | Description                                                                          | HTTP-code | This error occurs, if...                                                         |
|----|---------------------------|--------------------------------------------------------------------------------------|-----------|----------------------------------------------------------------------------------|
| 0  | Missing api token         | Every request needs the api token as header value.                                   | 403       | the header value `X-Api-Token` is missing.                                       |
| 1  | Database exception        | An exception occurred while interacting with the database. Please inform an admin.   | 500       | the database throws an exception. If this happends, please contact an developer. |
| 2  | Invalid paste             | Your paste has an invalid format. Please checkout the wiki for detailed information. | 400       | the paste doesn't follows the template `content`: "paste content".               |
| 3  | Paste too long            | Your paste is too long. You are only allowed to have 40000 chars.                    | 400       | if the content of the paste has more characters than 40.000.                     |
| 4  | Internal error            | An internal error occurred. Please inform an admin.                                  | 500       | if an unexpected exception is thrown. Please inform a developer about it.       |
| 5  | Invalid api token         | Your entered api token is not valid. Please check it again.                          | 403       | the given api token doesn't exist. Please check for typos.                       |
| 6  | Limit reached             | You are only allowed to send 20 requests per minute.                                 | 429       | you send more than 20 requests per minute.                                       |
| 7  | Invalid url               | This url is not supported.                                                           | 404       | the url isn't supported yet. Check for missing `/`.                              |
| 8  | Invalid method            | This method is not supported.                                                        | 405       | your HTTP method (GET,..) isn't supported on this url.                           |
| 9  | Api already closed        | The api client was already closed.                                                   | 400       | you are using the async client and you have already closed the executer.         |
| 10 | Request wasn't successful | Is the server offline?                                                               | 503       | the request processing wasn't successful. Maybe the server is down?              |

### Pastes
Every paste has the following format:  
```json
{
  "code": "m0m5hc8",
  "author": "Paul2708",
  "content": "This is a sample paste.",
  "timestamp": 1530968181000
}
```
`code` is the unique paste id, that will be used in the url. (e.g.: https://sbin.tk/?id=m0m5hc8)  
`author` is the name of the author relating to the owner of the api token.  
`content` represents the content of the paste.  
`timestamp` is the date, the paste was created in milliseconds from 1. January 1970.

## Common - Contact
If you find any issues, please [contact](../README.md) us!

