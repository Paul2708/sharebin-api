package de.paul2708.server.logger;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.LogRecord;

/**
 * This formatter formats the logger output in console and file.
 *
 * @author Paul2708
 */
public class LoggerFormat extends Formatter {

    /**
     * Date format used in the log.
     */
    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy - HH:mm:ss");

    /**
     * Format a log record to the following format:
     * <code>[Date - Time] [Level]: Message</code> <br>
     * Example: <code>[19.07.2018 - 19:22] [INFO]: Server started..</code>
     * 
     * @param record log record
     * @return formatted log record
     */
    @Override
    public String format(LogRecord record) {
        String output = String.format("[%s] [%s]: %s\n",
                LoggerFormat.DATE_FORMAT.format(new Date(record.getMillis())),
                record.getLevel().equals(Level.SEVERE) ? "ERROR" : record.getLevel(),
                record.getMessage());
        
        if (record.getThrown() != null) {
            output += getStackTrace(record.getThrown());
        }
        
        return output;
    }

    /**
     * Get the stack trace of an throwable as string.
     * 
     * @param throwable exception throwable
     * @return stack trace as string
     */
    private String getStackTrace(Throwable throwable) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw, true);
        throwable.printStackTrace(pw);

        return sw.getBuffer().toString();
    }
}
