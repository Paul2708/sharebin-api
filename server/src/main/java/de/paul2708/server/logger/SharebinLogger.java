package de.paul2708.server.logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class represents a logger with custom format and file handler.
 *
 * @author Paul2708
 */
public final class SharebinLogger {
    
    private static final String NAME = "SharebinAPI";
    
    private static final String PATH = "log/";
    
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy HH-mm-ss");
    
    private static final long TIME_DIFF = TimeUnit.DAYS.toMillis(5);

    private static final String FILE_NAME = "latest.log";
    
    private static Logger logger;

    /**
     * Nothing to call here..
     */
    private SharebinLogger() {
        throw new IllegalAccessError();
    }
    
    /**
     * Initialize the logger by creating the output file and adding the handlers.
     * 
     * @throws IOException if the file creation throws any exceptions
     */
    public static void initialize() throws IOException {
        SharebinLogger.logger = Logger.getLogger(SharebinLogger.NAME);

        // Add console handler
        ConsoleHandler consoleHandler = new ConsoleHandler();
        consoleHandler.setFormatter(new LoggerFormat());

        SharebinLogger.logger.addHandler(consoleHandler);

        // Create log directory or clear lock files
        if (Files.notExists(Paths.get(SharebinLogger.PATH))) {
            Files.createDirectories(Paths.get(SharebinLogger.PATH));
        } else {
            SharebinLogger.clearLockFiles();
            SharebinLogger.renameAndDeleteOlderLogs();
        }
        
        SharebinLogger.logger.setUseParentHandlers(false);
        
        // Add file handler
        FileHandler fileHandler = new FileHandler(SharebinLogger.PATH + SharebinLogger.FILE_NAME);
        fileHandler.setFormatter(new LoggerFormat());
        
        SharebinLogger.logger.addHandler(fileHandler);

        // Catch uncaught exceptions
        Thread.setDefaultUncaughtExceptionHandler((thread, throwable) -> {
            SharebinLogger.error("Uncaught exception in thread " + thread.getName(), throwable);
        });
    }

    /**
     * Log a message with level <code>INFO</code>.
     * 
     * @param message message to log
     */
    public static void info(String message) {
        logger.log(Level.INFO, message);
    }

    /**
     * Log a warning with level <code>WARNING</code> and exception.
     * 
     * @param message warn message
     * @param throwable exception throwable
     */
    public static void warn(String message, Throwable throwable) {
        logger.log(Level.WARNING, message, throwable);
    }

    /**
     * Log a warning with level <code>WARNING</code>.
     * 
     * @param message warn message
     */
    public static void warn(String message) {
        logger.log(Level.WARNING, message);
    }
    
    /**
     * Log an error with message and the exception throwable.
     * 
     * @param message error message
     * @param throwable exception throwable
     */
    public static void error(String message, Throwable throwable) {
        logger.log(Level.SEVERE, message, throwable);
    }

    /**
     * Log an error with message and level <code>SERVE</code>.
     * 
     * @param message error message
     */
    public static void error(String message) {
        logger.log(Level.SEVERE, message);
    }

    /**
     * Clear all <oode>.lck</oode> files.
     */
    private static void clearLockFiles() {
        for (File lockFile : Paths.get(SharebinLogger.PATH).toFile().listFiles()) {
            if (lockFile.getName().contains(".lck")) {
                try {
                    Files.delete(lockFile.toPath());
                } catch (IOException e) {
                    SharebinLogger.error("Error while deleting lock files.", e);
                }
            }
        }
    }

    /**
     * Rename latest log and delete older logs.
     */
    private static void renameAndDeleteOlderLogs() {
        for (File file : Paths.get(SharebinLogger.PATH).toFile().listFiles()) {
            try {
                // Reading line
                BufferedReader reader = new BufferedReader(new FileReader(file));
                String line = reader.readLine();
                reader.close();

                // Parsing time
                String date = line.split("\\ \\[")[0].replaceAll("\\[", "").replaceAll("\\]", "");
                Date created = LoggerFormat.DATE_FORMAT.parse(date);

                // Rename and delete
                if (file.getName().equals(SharebinLogger.FILE_NAME)) {
                    Files.move(file.toPath(),
                            Paths.get(SharebinLogger.PATH + SharebinLogger.DATE_FORMAT.format(created) + ".log"));
                } else {
                    if (created.getTime() + SharebinLogger.TIME_DIFF < System.currentTimeMillis()) {
                        Files.delete(file.toPath());
                    }
                }
            } catch (IOException | ParseException e) {
                SharebinLogger.error("Error while reading time.", e);
            }
        }
    }
}
