package de.paul2708.server.logger;

import org.eclipse.jetty.util.log.Logger;

/**
 * This class adapts a jetty logger to log the messages with {@link SharebinLogger}.
 *
 * @author Paul2708
 */
public class JettyLoggerAdapter implements Logger {
    
    private static final String NAME = "Jetty Logger";
    
    private static final String PREFIX = "Jetty >> ";

    /**
     * Get the name of the logger.
     * 
     * @return {@link #NAME}
     */
    @Override
    public String getName() {
        return JettyLoggerAdapter.NAME;
    }

    /**
     * Log a warning message.
     * 
     * @param s message
     * @param objects parameter objects
     */
    @Override
    public void warn(String s, Object... objects) {
        SharebinLogger.warn(PREFIX + insertObjects(s, objects));
    }

    /**
     * Warn with a throwable.
     * 
     * @param throwable exception throwable
     */
    @Override
    public void warn(Throwable throwable) {
        SharebinLogger.warn(PREFIX + "An exception was thrown:", throwable);
    }

    /**
     * Warn with message and throwable.
     * 
     * @param s message
     * @param throwable exception throwable
     */
    @Override
    public void warn(String s, Throwable throwable) {
        SharebinLogger.warn(PREFIX + s, throwable);
    }

    /**
     * Log a simple info message.
     * 
     * @param s message
     * @param objects parameter objects
     */
    @Override
    public void info(String s, Object... objects) {
        SharebinLogger.info(PREFIX + insertObjects(s, objects));
    }

    /**
     * Log a throwable as info message.
     * 
     * @param throwable exception throwable
     */
    @Override
    public void info(Throwable throwable) {
        SharebinLogger.warn(PREFIX + "Info(?) exception was thrown:", throwable);
    }

    /**
     * Log a info message with a throwable.
     * 
     * @param s message
     * @param throwable exception throwable
     */
    @Override
    public void info(String s, Throwable throwable) {
        SharebinLogger.warn(PREFIX + s, throwable);
    }

    /**
     * Return if debug is enabled. Here it is <code>false</code>
     * 
     * @return false
     */
    @Override
    public boolean isDebugEnabled() {
        return false;
    }

    /**
     * Set the debug enabled. As debug isn't enabled, we won't change it.
     * 
     * @param b boolean
     */
    @Override
    public void setDebugEnabled(boolean b) {
        // Nothing to do here
    }

    /**
     * Debug a message.
     * 
     * @param s message
     * @param objects parameter objects
     */
    @Override
    public void debug(String s, Object... objects) {
        // Nothing to do here
    }

    /**
     * Debug a message with a long.
     * 
     * @param s message
     * @param l long value
     */
    @Override
    public void debug(String s, long l) {
        // Nothing to do here
    }

    /**
     * Debug an throwable.
     * 
     * @param throwable exception throwable
     */
    @Override
    public void debug(Throwable throwable) {
        // Nothing to do here
    }

    /**
     * Debug a message and a throwable.
     * 
     * @param s message
     * @param throwable exception throwable
     */
    @Override
    public void debug(String s, Throwable throwable) {
        // Nothing to do here
    }

    /**
     * Return <code>this</code> for every input.
     * 
     * @param s name
     * @return <code>this</code>
     */
    @Override
    public Logger getLogger(String s) {
        return this;
    }

    /**
     * Ignore an throwable.
     * 
     * @param throwable exception throwable
     */
    @Override
    public void ignore(Throwable throwable) {
        // Nothing to do here
    }

    /**
     * Replace every <code>{}</code> with an object.
     * 
     * @param input input message
     * @param objects parameter objects
     * @return parameterized message
     */
    private String insertObjects(String input, Object... objects) {
        if (objects == null || objects.length == 0) {
            return input;
        }

        String result = input;
        for (int i = 0; i < objects.length; i++) {
            result = result.replaceFirst("\\{}", objects[i].toString());
        }
        
        return result;
    }
}
