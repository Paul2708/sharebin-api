package de.paul2708.server.database;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import de.paul2708.common.paste.Paste;
import de.paul2708.server.logger.SharebinLogger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * This database is a mysql implementation.
 *
 * @author Paul2708
 */
public class MySQLDatabase implements Database {

    private static final String ALLOWED_CHARS = "0123456789abcdefghijklmnopqrstuvwxyz";

    private static final int CODE_LENGTH = 7;

    private String ip;
    private String port;
    private String user;
    private String password;
    
    private HikariDataSource acpDataSource;
    private HikariDataSource pasteDataSource;
    
    /**
     * Create a new mysql database.
     *
     * @param ip ip
     * @param port port
     * @param user user name
     * @param password users password
     */
    public MySQLDatabase(String ip, String port, String user, String password) {
        this.ip = ip;
        this.port = port;
        this.user = user;
        this.password = password;
    }

    /**
     * Connect to the database.
     *
     * @throws de.paul2708.server.database.DatabaseException if an exception is thrown
     */
    @Override
    public void connect() throws DatabaseException {
        this.acpDataSource = new HikariDataSource(createConfig("sharebin_interface"));
        this.pasteDataSource = new HikariDataSource(createConfig("sharebin_content"));
    }

    /**
     * Set up the database connection.
     *
     * @throws de.paul2708.server.database.DatabaseException if an exception is thrown
     */
    @Override
    public void setUp() throws DatabaseException {
        // Nothing to do here
    }

    /**
     * Check if an api token exists.
     *
     * @param token api token to check
     * @return true if the token exists, otherwise false
     * @throws de.paul2708.server.database.DatabaseException if an exception is thrown
     */
    @Override
    public boolean exist(String token) throws DatabaseException {
        try (Connection connection = acpDataSource.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM `" + "api" + "` WHERE `api_key` = ?");

            statement.setString(1, token);

            ResultSet resultSet = statement.executeQuery();

            return resultSet.next();
        } catch (SQLException e) {
            SharebinLogger.error("Error while checking if a token exists in the database", e);

            throw new DatabaseException("An error occurred while checking the token.", e.getCause());
        }
    }

    /**
     * Resolve all pastes from a certain api token.
     *
     * @param token api token
     * @return a list of pastes
     * @throws de.paul2708.server.database.DatabaseException if an exception is thrown
     */
    @Override
    public List<Paste> resolvePastes(String token) throws DatabaseException {
        try (Connection connection = pasteDataSource.getConnection()) {
            String user = getUserFromToken(token);

            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM `" + "pastes" + "` WHERE `owner` = ?");

            statement.setString(1, user);

            ResultSet resultSet = statement.executeQuery();

            List<Paste> pastes = new LinkedList<>();

            while (resultSet.next()) {
                String id = resultSet.getString("id");
                String content = resultSet.getString("content");
                String timeStamp = resultSet.getString("created");
                String author = resultSet.getString("owner");

                long realTimeStamp = TimeUnit.SECONDS.toMillis(Long.valueOf(timeStamp));

                Paste paste = new Paste(id, author, realTimeStamp, content);
                pastes.add(paste);
            }

            return pastes;
        } catch (SQLException e) {
            SharebinLogger.error("Error while resolving pastes from the database", e);

            throw new DatabaseException("An error occurred while resolving pastes.", e.getCause());
        }
    }

    /**
     * Add a paste to the database.
     *
     * @param token api token
     * @param paste paste content
     * @return the generated paste
     * @throws de.paul2708.server.database.DatabaseException if an exception is thrown
     */
    @Override
    public Paste addPaste(String token, String paste) throws DatabaseException {
        try (Connection connection = pasteDataSource.getConnection()) {
            String user = getUserFromToken(token);
            String code = generateKey();
            long timeStamp = System.currentTimeMillis();
            long databaseTimeStamp = TimeUnit.MILLISECONDS.toSeconds(timeStamp);

            PreparedStatement update = connection.prepareStatement(
                    "INSERT IGNORE INTO `" +  "pastes" + "` ("
                            + "`id`, `content`, `created`, `owner`, `allowed`, `ip`, `clicks`"
                            + ") VALUES ("
                            + "?, ?, ?, ?, ?, ?, ?"
                            + ")");

            update.setString(1, code);
            update.setString(2, paste);
            update.setString(3, String.valueOf(databaseTimeStamp));
            update.setString(4, user);
            update.setString(5, "null");
            update.setString(6, "paste by rest service");
            update.setInt(7, 0);

            update.executeUpdate();

            return new Paste(code, user, timeStamp, paste);
        } catch (SQLException e) {
            SharebinLogger.error("Error while adding a paste to the database", e);

            throw new DatabaseException("An error occurred while pasting a paste.", e.getCause());
        }
    }

    /**
     * Get the user name by api token.
     *
     * @param token api token
     * @return user name
     * @throws DatabaseException if an exception is thrown
     */
    private String getUserFromToken(String token) throws DatabaseException {
        try (Connection connection = acpDataSource.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM `" + "api" + "` WHERE `api_key` = ?");

            statement.setString(1, token);

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                return resultSet.getString("user");
            }

            throw new DatabaseException("This token doesn't exist.");
        } catch (SQLException e) {
            SharebinLogger.error("Error while getting the user name from a token from the database", e);

            throw new DatabaseException("An error occurred while getting the user from token.",
                    e.getCause());
        }
    }

    /**
     * Generate a random key.
     *
     * @return key
     */
    private String generateKey() {
        Random random = new Random();
        StringBuilder code = new StringBuilder();

        for (int i = 0; i < MySQLDatabase.CODE_LENGTH; i++) {
            code.append(MySQLDatabase.ALLOWED_CHARS.charAt(random.nextInt(MySQLDatabase.ALLOWED_CHARS.length())));
        }

        return code.toString();
    }

    /**
     * Create a new hikari config by a database name
     * 
     * @param database name
     * @return new hikari database
     */
    private HikariConfig createConfig(String database) {
        // Setup pool config
        HikariConfig config = new HikariConfig();

        config.setDriverClassName("com.mysql.cj.jdbc.Driver");
        config.setJdbcUrl("jdbc:mysql://" + this.ip + ":" + this.port + "/" + database + "?serverTimezone=UTC");

        config.setUsername(this.user);
        config.setPassword(this.password);
        config.setPoolName("Sharebin-Pool-" + database);

        // Recommend pool settings
        config.addDataSourceProperty("cachePrepStmts", "true");
        config.addDataSourceProperty("prepStmtCacheSize", "250");
        config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
        config.setMaximumPoolSize(10);
        config.setConnectionTimeout(2000);
        config.setConnectionTestQuery("SELECT 1");
        config.setLeakDetectionThreshold(60000);

        return config;
    }
}
