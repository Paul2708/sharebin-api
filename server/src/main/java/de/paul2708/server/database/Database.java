package de.paul2708.server.database;

import de.paul2708.common.paste.Paste;
import java.util.List;

/**
 * This interface represents a database and is used as general strategy.
 *
 * @author Paul2708
 */
public interface Database {

    /**
     * Connect to the database.
     *
     * @throws DatabaseException if an exception is thrown
     */
    void connect() throws DatabaseException;

    /**
     * Set up the database connection.
     *
     * @throws DatabaseException if an exception is thrown
     */
    void setUp() throws DatabaseException;

    /**
     * Check if an api token exists.
     *
     * @param token api token to check
     * @return true if the token exists, otherwise false
     * @throws DatabaseException if an exception is thrown
     */
    boolean exist(String token) throws DatabaseException;

    /**
     * Resolve all pastes from a certain api token.
     *
     * @param token api token
     * @return a list of pastes
     * @throws DatabaseException if an exception is thrown
     */
    List<Paste> resolvePastes(String token) throws DatabaseException;

    /**
     * Add a paste to the database.
     *
     * @param token api token
     * @param paste paste content
     * @return the generated paste
     * @throws DatabaseException if an exception is thrown
     */
    Paste addPaste(String token, String paste) throws DatabaseException;
}
