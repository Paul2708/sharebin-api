package de.paul2708.server.database;

import de.paul2708.common.paste.Paste;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * This database is local and doesn't save any data.
 *
 * @author Paul2708
 */
public class LocalDatabase implements Database {

    // Map user name and api token
    private Map<String, String> users;

    // Map api token and its pastes
    private Map<String, List<Paste>> pastes;

    /**
     * Connect to the database.
     */
    @Override
    public void connect() {
        // Nothing to do here
    }

    /**
     * Set up the database connection.
     */
    @Override
    public void setUp() {
        this.users = new HashMap<>();
        this.users.put("TestUser", "0123456789");

        this.pastes = new HashMap<>();
        this.pastes.put("0123456789", new ArrayList<>());
    }

    /**
     * Check if an api token exists.
     *
     * @param token api token to check
     * @return true if the token exists, otherwise false
     */
    @Override
    public boolean exist(String token) {
        return users.containsValue(token);
    }

    /**
     * Resolve all pastes from a certain api token.
     *
     * @param token api token
     * @return a list of pastes
     */
    @Override
    public List<Paste> resolvePastes(String token) {
        return pastes.get(token);
    }

    /**
     * Add a paste to the database.
     *
     * @param token api token
     * @param paste paste content
     * @return the generated paste
     */
    @Override
    public Paste addPaste(String token, String paste) {
        long timeStamp = System.currentTimeMillis();
        Paste created = new Paste(getUniqueCode(), getUser(token), timeStamp, paste);

        List<Paste> pasteList = pastes.get(token);
        pasteList.add(created);
        pastes.put(token, pasteList);

        return created;
    }

    /**
     * Get an unique paste code by an uuid.
     * An infinity loop will not exist as only three users use sharebin.
     *
     * @return unique code
     */
    private String getUniqueCode() {
        UUID uuid = UUID.randomUUID();

        for (String token : users.values()) {
            if (uuid.toString().equalsIgnoreCase(token)) {
                return getUniqueCode();
            }
        }

        return uuid.toString();
    }

    /**
     * Get the user name by api-token.
     *
     * @param token api token
     * @return the user name or <code>null</code> if the user doesn't exist
     */
    private String getUser(String token) {
        for (Map.Entry<String, String> entry : users.entrySet()) {
            if (entry.getValue().equals(token)) {
                return entry.getKey();
            }
        }

        return null;
    }
}
