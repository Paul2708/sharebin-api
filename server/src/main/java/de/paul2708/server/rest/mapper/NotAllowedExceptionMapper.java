package de.paul2708.server.rest.mapper;

import de.paul2708.common.rest.response.ErrorResponse;
import de.paul2708.server.rest.util.Utility;

import javax.ws.rs.NotAllowedException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * This exception mapper handles an 405 error.
 *
 * @author Paul2708
 */
@Provider
public class NotAllowedExceptionMapper implements ExceptionMapper<NotAllowedException> {

    /**
     * Return an invalid method response.
     *
     * @param e not allowed exception
     * @return invalid url response
     */
    @Override
    public Response toResponse(NotAllowedException e) {
        return Utility.build(ErrorResponse.INVALID_METHOD);
    }
}
