package de.paul2708.server.rest.util;

import de.paul2708.common.rest.response.ResourceResponse;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * This class provides some utility methods.
 *
 * @author Paul2708
 */
public final class Utility {

    /**
     * Private constructor.
     */
    private Utility() {
        throw new IllegalAccessError();
    }

    /**
     * Build a {@link Response} based on a {@link ResourceResponse}.
     *
     * @param resourceResponse resource response
     * @return jax rs response
     */
    public static Response build(ResourceResponse resourceResponse) {
        return Response.status(resourceResponse.getHtmlStatus()).type(MediaType.APPLICATION_JSON)
                .entity(resourceResponse.createJsonResponse().toJson()).build();
    }
}
