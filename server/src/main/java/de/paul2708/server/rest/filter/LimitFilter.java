package de.paul2708.server.rest.filter;

import de.paul2708.common.rest.response.ErrorResponse;
import de.paul2708.server.logger.SharebinLogger;
import de.paul2708.server.rest.request.RequestCache;
import de.paul2708.server.rest.resource.PasteResource;
import de.paul2708.server.rest.util.Utility;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.ext.Provider;
import java.util.List;

/**
 * This filter filters every request and checks the request per minute limit.
 *
 * @author Paul2708
 */
@Provider
@PreMatching
public class LimitFilter implements ContainerRequestFilter {

    @Context
    private HttpHeaders httpHeaders;

    @Context
    private HttpServletRequest request;

    /**
     * Filter the valid requests.
     *
     * @param context request context
     */
    @Override
    public void filter(ContainerRequestContext context) {
        String token = getToken();

        if (token == null) {
            context.abortWith(Utility.build(ErrorResponse.MISSING_API_TOKEN));
            return;
        }

        RequestCache cache = RequestCache.getInstance();

        if (!cache.handleRequest(token)) {
            SharebinLogger.info(String.format("%s has reached the limit.", request.getRemoteAddr()));
            
            context.abortWith(Utility.build(ErrorResponse.LIMIT_REACHED));
        }
    }

    /**
     * Get the api token.
     *
     * @return api token, or <code>null</code> if there is no api token header
     */
    private String getToken() {
        List<String> headers = httpHeaders.getRequestHeader(PasteResource.HEADER_KEY);

        if (headers == null || headers.get(0) == null) {
            return null;
        }

        return headers.get(0);
    }
}
