package de.paul2708.server.rest.request;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * This singleton is used to cache requests to check the limit.
 *
 * @author Paul2708
 */
public final class RequestCache {

    private static final RequestCache INSTANCE = new RequestCache();

    /**
     * Amount of allowed requests per minute.
     */
    public static final int REQUESTS_PER_MINUTE = 20;

    private Map<String, Request> requests;

    /**
     * Create a new request cache with empty requests.
     */
    private RequestCache() {
        this.requests = new HashMap<>();
    }

    /**
     * Handle an incoming request.
     *
     * @param token request token
     * @return true if the request is valid, otherwise false
     */
    public boolean handleRequest(String token) {
        Request request = requests.get(token);

        // First request
        if (request ==  null || request.getTimeStamp() == -1) {
            addRequest(token);
            return true;
        }

        if (request.getCount() >= RequestCache.REQUESTS_PER_MINUTE) {
            if (request.getTimeStamp() + TimeUnit.MINUTES.toMillis(1) > System.currentTimeMillis()) {
                return false;
            } else {
                request.clear();
                request.request();
                return true;
            }
        }

        request.request();
        return true;
    }

    /**
     * Add a request.
     *
     * @param token api token
     */
    private void addRequest(String token) {
        Request request = requests.get(token);

        if (request == null) {
            request = new Request();

            requests.put(token, request);
        }

        request.request();
    }

    /**
     * Get the cache instance.
     *
     * @return request cache instance
     */
    public static RequestCache getInstance() {
        return INSTANCE;
    }
}
