package de.paul2708.server.rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * This class is the rest application and needs a main path.
 */
@ApplicationPath("")
public class RestApplication extends Application {

    // Nothing to do here anymore.
}
