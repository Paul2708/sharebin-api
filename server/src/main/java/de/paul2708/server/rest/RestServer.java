package de.paul2708.server.rest;

import de.paul2708.server.logger.JettyLoggerAdapter;
import de.paul2708.server.logger.SharebinLogger;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.util.log.Log;
import org.glassfish.jersey.servlet.ServletContainer;

import java.net.BindException;

/**
 * This server represents the rest server.
 *
 * @author Paul2708
 */
public class RestServer implements Runnable {

    private int port;

    /**
     * Create a new rest server with an specific port.
     * 
     * @param port rest port
     */
    public RestServer(int port) {
        this.port = port;
    }

    /**
     * Run the server.
     */
    @Override
    public void run() {
        // Set logger
        Log.setLog(new JettyLoggerAdapter());
        
        // Start server
        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setContextPath("/");

        Server jettyServer = new Server(this.port);
        jettyServer.setHandler(context);

        ServletHolder jerseyServlet = context.addServlet(ServletContainer.class, "/*");
        jerseyServlet.setInitOrder(0);

        jerseyServlet.setInitParameter(
                "jersey.config.server.provider.packages",
                "de.paul2708.server.rest");
    
        try {
            jettyServer.start();
            jettyServer.join();
        } catch (BindException e) {
            SharebinLogger.error("The port " + port + " is already in use. Maybe the server is still running?");
            
            System.exit(-1);
        } catch (Exception e) {
            SharebinLogger.error("Couldn't start the jetty server.", e);
        }
    }
}
