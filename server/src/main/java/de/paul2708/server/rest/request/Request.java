package de.paul2708.server.rest.request;

/**
 * This class represents a request and counts.
 *
 * @author Paul2708
 */
public class Request {

    private int count;
    private long timeStamp;

    /**
     * Create a new request.
     */
    public Request() {
        this.count = 0;
        this.timeStamp = -1;
    }

    /**
     * Count a request.
     */
    public void request() {
        if (count == 0) {
            this.timeStamp = System.currentTimeMillis();
        }

        this.count++;
    }

    /**
     * Clear the requests.
     */
    public void clear() {
        this.count = 0;
        this.timeStamp = -1;
    }

    /**
     * Get the total count of requests.
     *
     * @return count
     */
    public int getCount() {
        return count;
    }

    /**
     * Get the last request time stamp.
     *
     * @return time stamp
     */
    public long getTimeStamp() {
        return timeStamp;
    }
}
