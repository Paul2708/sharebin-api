package de.paul2708.server.rest.mapper;

import de.paul2708.common.rest.response.ErrorResponse;
import de.paul2708.server.rest.util.Utility;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * This exception mapper handles an 404 error.
 *
 * @author Paul2708
 */
@Provider
public class NotFoundExceptionMapper implements ExceptionMapper<NotFoundException> {

    /**
     * Return an invalid url response.
     *
     * @param e not found exception
     * @return invalid url response
     */
    @Override
    public Response toResponse(NotFoundException e) {
        return Utility.build(ErrorResponse.INVALID_URL);
    }
}
