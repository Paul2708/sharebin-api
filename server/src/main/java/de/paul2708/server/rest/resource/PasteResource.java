package de.paul2708.server.rest.resource;

import de.paul2708.common.paste.Paste;
import de.paul2708.common.rest.response.ErrorResponse;
import de.paul2708.common.rest.response.PasteCreationResponse;
import de.paul2708.common.rest.response.PasteListResponse;
import de.paul2708.server.SharebinServer;
import de.paul2708.server.database.Database;
import de.paul2708.server.database.DatabaseException;
import de.paul2708.server.logger.SharebinLogger;
import de.paul2708.server.rest.util.Utility;
import org.json.simple.JsonObject;
import org.json.simple.Jsoner;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * This resource handles get and create paste requests.
 *
 * @author Paul2708
 */
@Path("/pastes")
@Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
public class PasteResource {

    public static final String HEADER_KEY = "X-Api-Token";

    private static final String CONTENT_KEY = "content";

    @Context
    private HttpHeaders httpHeaders;

    /**
     * Resolve all pastes by using GET.
     *
     * @return detailed response
     */
    @GET
    public Response resolvePastes() {
        String token = getToken();

        // Get token
        if (token == null) {
            return Utility.build(ErrorResponse.MISSING_API_TOKEN);
        }

        Database database = SharebinServer.getInstance().getDatabase();

        // Check token
        try {
            if (!database.exist(token)) {
                return Utility.build(ErrorResponse.INVALID_API_TOKEN);
            }
        } catch (DatabaseException e) {
            SharebinLogger.error("Error while resolving pastes", e);
            
            return Utility.build(ErrorResponse.DATABASE_EXCEPTION);
        }

        // List pastes
        try {
            List<Paste> pastes = database.resolvePastes(token);

            SharebinLogger.info(String.format("%s resolved its pastes.", token));
            
            return Utility.build(new PasteListResponse(pastes));
        } catch (DatabaseException e) {
            SharebinLogger.error("Error while resolving pastes", e);
            
            return Utility.build(ErrorResponse.DATABASE_EXCEPTION);
        }
    }

    /**
     * Get the api token.
     *
     * @return api token, or <code>null</code> if there is no api token header
     */
    private String getToken() {
        List<String> headers = httpHeaders.getRequestHeader(PasteResource.HEADER_KEY);

        if (headers == null || headers.get(0) == null) {
            return null;
        }

        return headers.get(0);
    }

    /**
     * Create a new paste by using POST.
     *
     * @param jsonPaste paste as json
     * @return detailed response
     */
    @Path("/create")
    @POST
    public Response createPaste(String jsonPaste) {
        // Check if paste is valid
        JsonObject object = Jsoner.deserialize(jsonPaste, new JsonObject());

        if (!isValidPaste(object)) {
            return Utility.build(ErrorResponse.INVALID_PASTE);
        }

        // Check content
        String content = object.getString("content");

        if (content.length() > Paste.MAX_LENGTH) {
            return Utility.build(ErrorResponse.PASTE_TOO_LONG);
        }

        Database database = SharebinServer.getInstance().getDatabase();

        // Check token
        String token = getToken();

        if (token != null) {
            try {
                if (!database.exist(token)) {
                    return Utility.build(ErrorResponse.INVALID_API_TOKEN);
                }
            } catch (DatabaseException e) {
                SharebinLogger.error("Error while creating a paste", e);
                
                return Utility.build(ErrorResponse.DATABASE_EXCEPTION);
            }
        } else {
            return Utility.build(ErrorResponse.MISSING_API_TOKEN);
        }

        // Paste
        try {
            Paste paste = database.addPaste(token, content);

            SharebinLogger.info(String.format("%s created a paste.", paste.getAuthor()));
            
            return Utility.build(new PasteCreationResponse(paste));
        } catch (DatabaseException e) {
            SharebinLogger.error("Error while creating a paste", e);
            
            return Utility.build(ErrorResponse.DATABASE_EXCEPTION);
        }
    }

    /**
     * Check if a given json object is a valid paste.
     *
     * @param object object to test
     * @return true if the object is valid, otherwise false
     */
    private boolean isValidPaste(JsonObject object) {
        if (object == null || object.size() != 1) {
            return false;
        }

        return object.containsKey(PasteResource.CONTENT_KEY)
                && object.getString(PasteResource.CONTENT_KEY) != null;
    }
}
