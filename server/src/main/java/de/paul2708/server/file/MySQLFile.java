package de.paul2708.server.file;

import de.paul2708.common.file.InvalidValueException;
import de.paul2708.common.file.PropertiesFile;
import java.util.Properties;

/**
 * This file is the configuration file.
 *
 * @author Paul2708
 */
public class MySQLFile extends PropertiesFile {

    private static final String PATH = "./database/mysql.properties";

    private static final String COMMENT = "MySQL properties";

    /**
     * Create a new configuration file.
     */
    public MySQLFile() {
        super(MySQLFile.PATH, MySQLFile.COMMENT);
    }

    /**
     * Load the default properties value.
     *
     * @return default properties value
     */
    @Override
    public Properties defaultValue() {
        Properties properties = new Properties();

        properties.setProperty("ip", "127.0.0.1");
        properties.setProperty("port", "3306");
        properties.setProperty("user", "root");
        properties.setProperty("password", "123456789");

        return properties;
    }

    /**
     * Validate the values.
     *
     * @throws InvalidValueException if an value isn't valid
     */
    @Override
    public void validate() throws InvalidValueException {
        if (get("ip") == null || get("port") == null
                || get("user") == null || get("password") == null) {
            throw new InvalidValueException("all keys", "ip, port, user and password have to be properties");
        }

        // Check port
        try {
            if (Integer.parseInt(get("port")) < 0) {
                throw new InvalidValueException("port", "the port has to be positive");
            }
        } catch (NumberFormatException e) {
            throw new InvalidValueException("port", "the port should be a number");
        }
    }
}
