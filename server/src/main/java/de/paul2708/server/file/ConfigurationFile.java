package de.paul2708.server.file;

import de.paul2708.common.file.InvalidValueException;
import de.paul2708.common.file.PropertiesFile;
import java.util.Properties;

/**
 * This file is the configuration file.
 *
 * @author Paul2708
 */
public class ConfigurationFile extends PropertiesFile {

    private static final String PATH = "config.properties";

    private static final String COMMENT = "Configure the server";

    /**
     * Create a new configuration file.
     */
    public ConfigurationFile() {
        super(ConfigurationFile.PATH, ConfigurationFile.COMMENT);
    }

    /**
     * Load the default properties value.
     *
     * @return default properties value
     */
    @Override
    public Properties defaultValue() {
        Properties properties = new Properties();

        properties.setProperty("database", "mysql");
        properties.setProperty("rest_port", "8080");

        return properties;
    }

    /**
     * Validate the values.
     * 
     * @throws InvalidValueException if an value isn't valid
     */
    @Override
    public void validate() throws InvalidValueException {
        if (get("database") == null || get("rest_port") == null) {
            throw new InvalidValueException("all keys", "database and rest_port have to be properties");
        }
        
        // Check database
        if (!get("database").equalsIgnoreCase("mysql") && !get("database").equalsIgnoreCase("local")) {
            throw new InvalidValueException("database", "database has to be mysql or local");
        }
        
        // Check port
        try {
            if (Integer.parseInt(get("rest_port")) < 0) {
                throw new InvalidValueException("rest_port", "the port has to be positive");
            }
        } catch (NumberFormatException e) {
            throw new InvalidValueException("rest_port", "the port should be a number");
        }
    }
}
