package de.paul2708.server;

import de.paul2708.common.file.InvalidValueException;
import de.paul2708.common.file.PropertiesFile;
import de.paul2708.server.database.Database;
import de.paul2708.server.database.DatabaseException;
import de.paul2708.server.database.LocalDatabase;
import de.paul2708.server.database.MySQLDatabase;
import de.paul2708.server.file.ConfigurationFile;
import de.paul2708.server.file.MySQLFile;
import de.paul2708.server.logger.SharebinLogger;
import de.paul2708.server.rest.RestServer;

import java.io.IOException;

/**
 * This class represents the sharebin server with configs, database and rest service.
 * It is used as singleton to get access to current files and database.
 *
 * @author Paul2708
 */
public final class SharebinServer {

    private static final SharebinServer INSTANCE = new SharebinServer();

    private Database database;

    /**
     * Private default constructor.
     */
    private SharebinServer() {

    }

    /**
     * Load configs, connect to database and start rest service.
     */
    public void start() {
        // Load logger
        try {
            SharebinLogger.initialize();
        } catch (IOException e) {
            // Cannot use logger to log...
            e.printStackTrace();
        }

        SharebinLogger.info("Loading properties files..");
        
        // Load files
        PropertiesFile configFile = new ConfigurationFile();
        MySQLFile mySQLFile = new MySQLFile();
        
        try {
            configFile.load();
            mySQLFile.load();
        } catch (InvalidValueException e) {
            SharebinLogger.error(e.getMessage());
            
            System.exit(-1);
        }

        SharebinLogger.info("All properties are correct and will be applied.");

        // Set database
        this.database = configFile.get("database").equalsIgnoreCase("mysql")
                ? new MySQLDatabase(mySQLFile.get("ip"), mySQLFile.get("port"), mySQLFile.get("user"),
                mySQLFile.get("password"))
                : new LocalDatabase();

        try {
            SharebinLogger.info("Connecting to the database..");
            
            this.database.connect();
            this.database.setUp();

            SharebinLogger.info("Successfully connected to the database.");
        } catch (DatabaseException e) {
            SharebinLogger.error("Error while connecting and setting up the database", e);
        }

        // Run rest service
        new Thread(new RestServer(Integer.parseInt(configFile.get("rest_port")))).start();
    }

    /**
     * Get the current database.
     *
     * @return database
     */
    public Database getDatabase() {
        return database;
    }

    /**
     * Get the sharebin server instance.
     *
     * @return instance
     */
    public static SharebinServer getInstance() {
        return INSTANCE;
    }
}
