package de.paul2708.server;

/**
 * This class is the main class and starts the server.
 *
 * @author Paul2708
 */
public final class Main {

    /**
     * Nothing to call here....
     */
    private Main() {
        throw new IllegalAccessError();
    }

    /**
     * The main method and program entry.
     *
     * @param args command line arguments
     */
    public static void main(String[] args) {
        SharebinServer server = SharebinServer.getInstance();
        server.start();
    }
}
