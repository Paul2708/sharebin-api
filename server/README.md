# Sharebin - Server
---
This project is the server part of the API and holds a REST service.

## Server - How does it work?
It basically uses [JAX-RS](https://jersey.github.io "Jersey Website") to setup the REST service and [Jetty](https://www.eclipse.org/jetty/ "Jetty Website") to use a HTTP-Server.  

Currently all the user data and pastes are stored in MySQL database. As the `Database` is a abstract strategy, it can be changed easily.

## Server - Contact
If you find any issues, please [contact](../README.md) us!