# Sharebin - API
---
This project is an interface for the paste service [Sharebin](https://sbin.tk "Sharebin Paste Service").

## Sharebin - About
Sharebin is a simple and fast paste service. It was created by [@L_Leon](https://twitter.com/L_LeonDE) and is still under heavy development.

## Sharebin - How to use?
Sharebin as paste service is very simple to use. You can paste anonymous on https://sbin.tk.  
Or you can register yourself on https://sbin.tk/interface/ and use multiple features:  

 -  post your content from the interface
 -  list all your postes and sort them
 -  edit existing pastes wihtout change the paste id
 -  delete existing pastes
 -  give access to other users to edit your pastes
 -  gain _access_ to the _API_

## API - Getting started
If you want to use the API, you need to get an API token.

 1.  Register yourself on https://sbin.tk/interface/ or log-in.
 2.  Switch to the [API-Tab](https://sbin.tk/interface/api) on the left side.
 3.  Create a new token.  

The created token will be used in your application to get access to the api interface.

### Using Java
If you want to use the API in your Java application, have a look at the [Sharebin-Client](client).

### Other languages
The api is based on a REST service.  
Every following request needs the api token as header value. Add a header `X-Api-Token` and set its value to your `api-token`.  
Note: The input and output content is always `application/json`.  

#### Resolve pastes
- Method: `GET`
- URL: `api.sbin.tk/pastes/`
- API-Header needed
- no further input needed
- Output: Json-Array with all your pastes
- Example-Output
```json
[
  {
    "code": "i4ghucq",
    "author": "Paul2708",
    "content": "Example paste",
    "timestamp": 1530968142000
  },
  {
    "code": "bgy0d92",
    "author": "Paul2708",
    "content": "Another paste",
    "timestamp": 1530568781000
  },
  {
    "code": "iwditt6",
    "author": "Paul2708",
    "content": "Checkout sbin.tk",
    "timestamp": 1530968100000
  }
]
```

#### Create paste
- Method: `POST`
- URL: `api.sbin.tk/pastes/create/`
- API-Header needed
- Input: Json object with `content` attribute
- Output: created json paste
- Example-Request

```json
{
  "content": "This is a sample paste. Check it out!"
}
```
```json
{
  "code": "03bkl7m",
  "author": "Paul2708",
  "content": "This is a sample paste. Check it out!",
  "timestamp": 1530968256300
}
```

### Notes
- You are only allowed to send 20 requests per minute.
- The maximum length of paste is limited to 40000 characters.
- You need your api key in every request.
- Checkout the possible errors [here](common).

## Contact
If you find any issues, please let us know! Also if you are using the service.

Paul2708 (api dev) - [BitBucket](https://bitbucket.org/Paul2708/) [Twitter](https://twitter.com/theplayerpaul) Discord: Paul2708#1098 [Mail](mailto:playerpaul2708@gmx.de)  
L_Leon (main dev) - [Twitter](https://twitter.com/L_LeonDE)
